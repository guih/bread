var app = angular.module('Bread', []);
app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
}])

app.service('authInterceptor', function($q) {
    var service = this;

    service.responseError = function(response) {
        if (response.status == 401){
            window.location = "/";
        }
        return $q.reject(response);
    };
})

app.service('OrdersExchange', function($rootScope, $http, $q) {

	var orders = {};
	var lock = true;
	function fetch(fn) {
		$http.post('/data', {
			data: { 
				_ : +new Date() 
			}
		}).then(function(response) {

			set({
				error: false,
				res: orders = response.data
			});

			fn();
		}, function() {
			set({
				error: true
			});
		});
	};
	function toggle(_flavor, fn) {

		// if( lock == true ){
			$http.post('/toggle', {
				data: { 
					flavor : _flavor
				}
			}).then(function(response) {

				setFlavor({
					error: false,
					res: {
						flavor: response.data
					}
				});
				// lock = false;

				fn();
			}, function() {
				setFlavor({
					error: true
				});
				fn();
			});			
		// }

	};
	function get(){
		return $rootScope.orders;
	}
	function set(_data){
		$rootScope.orders = _data;
	}
	function getFlavor(){
		return $rootScope.flavor;
	}
	function setFlavor(_data){
		// $rootScope.flavor = _flavor;
		var lock = true;

	}
	return {
		fetchOrders 	: fetch,
		toggleFlavor 	: toggle,
		setOrders 		: set,
		getOrders		: get
	}
});
app.service('UserHeartBeat', function($rootScope, $http, $q) {

	var user = {};

	function fetch(fn) {
		$http.post('/user', {
			data: { 
				_ : +new Date() 
			}
		}).then(function(response) {

			set({
				error: false,
				account: user = response.data
			});

			fn();
		}, function() {
			set({
				error: true
			});
		});
	};
	function get(){
		return $rootScope.user;
	}
	function set(_data){
		$rootScope.user = _data || user;
	}
	return {
		fetchUser: fetch,
		setUser: set,
		getUser: get
	}
});
app.controller('MainCtrl', function($q, $scope, $rootScope, OrdersExchange, UserHeartBeat) {
	$scope.orders = [];
	$scope.due = 0;
	$scope.salt = 0;
	$scope.sweet = 0;
	$scope.reverse = true;
	$scope.capaignId = 0;
	$scope.predicate = 'age';
	$scope.totalOrders = 0;
	$scope.peopleCount = 0;
	lock = false;

	UserHeartBeat.fetchUser(function(){
		$scope.user = UserHeartBeat.getUser();
		$scope.picture = $scope.user.account.user.picture.replace('?sz=50', '?sz=120');
	});



	$scope.changeTo = function(flavor){
		// if( $scope.flavor != flavor ){			
			// lock = true;	
			OrdersExchange.toggleFlavor(flavor, function(newFlavor){
				$scope.flavor = flavor;
				// lock = null;
			});
		// }	
	};

	OrdersExchange.fetchOrders(function(){
		var data   =  $rootScope.orders = OrdersExchange.getOrders();
		var res    = data.res;
		var orders = res.orders;
		var count  = res.count;
		var now    = new Date(orders.time);
		var due    = new Date(parseInt(now.setUTCHours(new Date().getUTCHours()+3).toString()));
		
		$scope.due         = count.saved;
		$scope.salt        = count.salt;
		$scope.sweet       = count.sweet;
		$scope.orders      = orders.current;
		$scope.capaignId   = orders.time;
		$scope.totalOrders = count.total;
		$scope.peopleCount = count.total + count.saved;
		$scope.flavor 	   = count.flavor;



		$scope.dueDate = [due.toDateString(), due.toTimeString()].join(" ");
	});


});

app.directive('askChart', function() {
	return {
		restrict: 'AE',
		replace: 'true',
		template: '<div class="graph"><div id="chart"></div></div>',
		link: function(scope, element, attrs){

			var charts = google.charts;
			var visualization = google.visualization;
			var chartElement = element.children()[0];

			charts.load('current', {
				'packages':['corechart']
			});

			charts.setOnLoadCallback(function(){
				
				var data = google.visualization.arrayToDataTable([
					['',				''],
					['Sweet',     		parseInt(attrs.sweet)],
					['Salt', 			parseInt(attrs.salt)],
					['Didn\'t choose', 	parseInt(attrs.due)]
				]);

				var options = {
					'title':'ORDERS',
					'width':250,
					'height':250,
					'pieHole': 0.4,
					'legend': 'none',
					'pieSliceTextStyle': {
						color: '#fff',
					},
					'slices': {
						0: { color: '#3498db' },
						1: { color: '#FF9800' },
						2: { color: '#2ecc71' }
					}
				};

				var chart = new google.visualization.PieChart(chartElement);
				chart.draw(data, options);
			});
		}
	};
});
app.directive('ordersTable', function($window, $rootScope) {
	return {	
		restrict: 'E',
		replace: 'false',
		scope: { 
			orders: '=' 
		},
		templateUrl: 'templates/orders-table.html',
		link: function(scope, element, attrs){
			
			scope.order = function(predicate) {
				scope.reverse = (scope.predicate === predicate) ? !scope.reverse : false;
				scope.predicate = predicate;
			};
			$window.addEventListener('scroll', onScrollHandler.bind(this));

			function onScrollHandler(){
				console.log( $rootScope.orders )
				if( $rootScope.orders.res.orders.current.length < 7)
					return;

				var tr = document.querySelector('thead').getElementsByTagName('tr')[0];
				var scrollTop = this.window.pageYOffset;
				var clsList = tr.classList;

				return scrollTop > 106 ? clsList.add('fix-it') : clsList.remove('fix-it')
			}
		}
	};
});