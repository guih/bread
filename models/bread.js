var mongoose = require('mongoose');  
var BreadList = new mongoose.Schema({  
	title: String,
	time: String,
	current: Array,
	completed: Boolean
});
mongoose.model('BreadList', BreadList);

