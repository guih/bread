var mongoose = require('mongoose');  
var user = new mongoose.Schema({  
	name: String,
	picture: String,
	emailDomain: String,
	email: String,
	level: Number,
	error: Boolean,
});
mongoose.model('User', user);