var email      = require('./email');
var utils      = require('./lib/util');
var user      = require('./lib/user');

var staticVars = require('./lib/static');
var db         = require('./models/db');
var mongoose   = require('mongoose');
var validator  = require('validator');
var bread      = require('./models/bread');
var breadlist  = mongoose.model('BreadList');
var express    = require('express');
var login      = require('./lib/passport');
var passport   = require('passport');
var session    = require('express-session');
var bodyParser = require('body-parser');
var path       = require('path');



var app = express();
app.use(session({
  secret: 'keyboard cat'
}));
passport.use(login.strategy);

app.use(passport.initialize());

app.use(passport.session());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); 

app.set('view engine', 'jade');
app.set('views', path.join(__dirname, 'views'));

app.use(express.static(path.join(__dirname, 'public')));
app.use(require('./routes'));

app.get('/confirm/:hash', function(req, res){

	var holder = [];
	var params = utils.atob( req.params.hash );
	var user = {};
	var userChunk = [];
	var users = staticVars.users;
	var holder = false;
	// res.set('Content-Type', 'text/html');
	res.setHeader('Content-Type', 'text/html');

	if( params ){
		userChunk = params.split(";");
		// console.log(user)

		for( var i = 0; i < users.length; i++ ){


			if( !validator.isEmail(userChunk[0])  ){
				res.render('confirm', {
					title: "Confirmação",
					className: "already",
					icon: "✘",
					message: "Opss!"
				});
				// return;
			}
			if( users[i].email ==  userChunk[0]){
				holder = true;
				user = {
					name : users[i].name,
					email: userChunk[0],
					type: userChunk[2]
				};	

				breadlist.findById(userChunk[1], function (err, bread) {
					if( err ){
						console.log(err);
					}

					for( var j = 0; j < bread.current.length; j++ ){
						// res.send(bread.current[i].email)
						if( bread.current[j].email == user.email ){
							res.render('confirm', {
								title: "Confirmação",
								className: "already",
								icon: "✘",
								message: "Seu pedido já foi computado."
							});
							// res.send("");
							return;
						}

						// if( bread. )
					}

					if( bread.completed == true ){
						res.render('confirm', {
							title: "Confirmação",
							className: "already",
							icon: "✘",
							message: "Hoje você não dá mais. Tente mais cedo na próxima!"
						});
						return;
					}
					// return;
					holder = bread.current;

					holder.push( user );
					bread.update({
						current: holder
					}, function (err, id){
						if(!err){
							res.render('confirm', {
								title: "Confirmação",
								className: "success",
								icon: "✔",
								message: "Obrigado! Seu pedido foi registrado."
							});
							return;
						}else{

						}
					});

				});
			
			}
			if( i+1 == users.length && holder == false ){
							// if( holder == false ){
				console.log('Unkown email');
			// }
			}
		}

	}
});

app.post('/toggle', function(req, res) {
	var flavor = req.body.data.flavor;
	var user = req.user;
	require('fs').readFile('./quee.txt', function (err, data) {

		if (err) throw err;
		var lines = data.toString().split("\n");
		var last = lines[lines.length-2];

		breadlist.find({_id:last}, function(err, Breads) {
			var Bread = Breads[0];


			for( var i = 0; i<Bread.current.length; i++ ){
				var order = Bread.current[i];
				if( order.email == user.email ){

					if( flavor == order.type ){
						return res.json({
							error: true,
							caption: "This is already your current flavor."
						});	
						return;
					}
					if( flavor != order.type ){						
						var newVal = {
							name: order.name,
							email: order.email,
							type: flavor
						};

						Bread.current.splice(i,1);
						Bread.current.push(newVal);
						Bread.save(function(err) {
							if (err) throw err;
							console.log(arguments[1].current, '~~~~~~~~~~~~~~~~~~');
							res.json({
								error: false,
								caption: "Succesfuly changed to "+flavor
							});
							
						});				
					}					
				}
			};

		});
	});
});
app.post('/data', function(req, res){
	if( req.user == undefined ){
		res.status(401).json({});
		return;		
	}

	require('fs').readFile('./quee.txt', function (err, data) {

		if (err) throw err;
		var lines = data.toString().split("\n");
		var last = lines[lines.length-2];
		
			breadlist.findById(last, function(err, breadList){
				user.getUserFlavor(breadList,req.user.email, function(flavor){	
				var salt =0 
				, sweet =0 
				, current = breadList.current;

				if( current.length == 0 ){
					res.json({
						orders: { 
							current: []
						},
						count: {

						}
					} );
					return;
				}
				for( var i = 0; i< current.length; i++ ){
					if( current[i].type == "sweet" ){
						sweet++;
					}else{
						salt++;				
					}

					if( i+1 == current.length ){
						res.json({
							orders: breadList, 
	    					count: {
			    				salt: salt, 
			    				sweet: sweet, 
			    				total: sweet + salt, 
			    				saved: (staticVars.users.length-1) -  (sweet+salt),
			    				capaignId: last,
			    				flavor: flavor ? flavor.type : null
		    				}
						});
					}
				}
			});
		});
	});
});

app.get('/orders', function(req, res){
	if( !req.user ){
		res.redirect('/');
		return;
	}
	if( req.user.error ){
		req.logout();
		res.redirect('/');
		return;
	}
	require('fs').readFile('./quee.txt', function (err, data) {
		if (err) throw err;
		var lines = data.toString().split("\n");
		var last = lines[lines.length-2];
		// // console.log(last)

		breadlist.findById(last, function(err, breadList){			
			// user.getUserFlavor(breadList,req.user.email, function(flavor){					
				var salt =0 , sweet =0 ;
				var current = breadList.current;
				if( current.length < 1 ){
					res.render('orders', { title: 'Orders', account : req.user, orders: { current: []}} );
					return;
				}
				for( var i = 0; i< current.length; i++ ){
					if( current[i].type == "sweet" ){
						sweet++;
					}else{
						salt++;				
					}

					if( i+1 == current.length ){
						// console.log(req.user, '-------------------------')
		    			res.render('orders', { 
	    					title: 'Ordersx', 
	    					orders: breadList, 
	    					count:{
								salt 	: salt, 
								sweet 	: sweet, 
								total 	: sweet + salt, 
								saved 	: (staticVars.users.length-1) -  (sweet+salt),
								// flavor 	: flavor ? flavor.type : null,
								account : req.user
		    				}
		    			});
					}
				}
			// });
		});
	});
});

app.get('/', function(req, res){
	if( !req.user ){
		res.render('home',{title: "Bread Manager"});
		
	}else{
		res.redirect('/orders')
	}
});
app.get('*', function(req, res){
	res.redirect('https://apiki.com');
});


app.listen(1337);

email.init(function(){

});


