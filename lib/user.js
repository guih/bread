var mongoose = require('mongoose');
var db       = require('../models/db');
var User     = require('../models/user');
var user     = mongoose.model('User');
var ObjectId = require('mongodb').ObjectID;






exports.getUsers = function(){
	
}
exports.getUser = function(userName, fn){
	user.find({ 'name': userName }, function (err, users) {
		if( !users){
			fn({
				error: true,
				msg: "Can't find user with this name."
			}, null);
			return;
		}

		fn(null, users[0]);
	});	
}

exports.getUserFlavor = function(list, email, fn){
	var current = list.current;
	for(var i = 0; i< current.length; i++){
		var userEmail = current[i].email;
		// console.log(userEmail == email, '))))))))))))))))))))))')
		if( userEmail == email ){
			fn(current[i]);
			return;
		}

		if( i+1 == current.length ){
			fn({});
		}
	}
}
exports.insertUser = function(userAccount, fn){
	user.create(userAccount, function(err, savedReport){
		if( err ){
			fn({
				error: true,
				msg: "Error while adding user."
			}, null);
			return;
		}
		fn(null, savedReport);
	});	
}
exports.checkUser = function(){
	
}