var express                 = require("express");
var app                     = express();
var passport                = require('passport');
var GoogleStrategy          = require('passport-google-oauth').OAuth2Strategy;


var staticVars              = require('../lib/static');
var account                 = require('../lib/user');



passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(obj, done) {
  done(null, obj);
});

exports.auth = passport.authenticate('google', { 
    scope: staticVars.CLIENT_SCOPE
});

exports.cb = passport.authenticate('google', { 
    failureRedirect: '#/login' 
});

exports.strategy = new GoogleStrategy({
        clientID        : staticVars.CLIENT_ID,
        clientSecret    : staticVars.CLIENT_SECRET,
        callbackURL     : staticVars.CALLBACK_URL
    },
    function(req, accessToken, refreshToken, profile, done) {
        process.nextTick(function () {
            var user = profile;


            var userMeta = user._json;
            // console.log(userMeta.emails[0].value, '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<------')
            var desiredUser = {
                name: userMeta.displayName,
                picture:  userMeta.image.url,
                emailDomain: userMeta.domain,  
                email: userMeta.emails[0].value,
                level: 0,
                error: null
            };
            if( userMeta.domain != "apiki.com" ){
                return done( null, {
                    error: "Your email domain don't match any of allowed ones."
                });
            }

            account.getUser( desiredUser.name, function(err, users){
               if( err ){
                    console.log(err);
                    return;
               }

                if( !users ){
                    account.insertUser( desiredUser, function(err, createdUser){
                        return done( null, desiredUser );
                    } );                
                }else{
                    return done( null, users );
                }
            } )
        });
    }
);
exports.ensureAuthenticated = function(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/login');
}
