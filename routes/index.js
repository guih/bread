var express = require('express');
var app = module.exports = express();
var login = require('../lib/passport');
var staticVars = require('../lib/static');
// var reports = require('../lib/reports');



// var customer = require('../models/report');
var mongoose = require('mongoose');


app.get('/auth/google', login.auth, function(req, res){});

app.get('/auth/google/callback', login.cb, function(req, res) {
    res.redirect('/orders');
});

app.get('/logout', function(req, res){
	req.logout();
	res.redirect('/');
});


app.get('/account', login.ensureAuthenticated, function(req, res){
	res.render('account', { 
		user: req.user 
	});
});


app.post('/user', function(req, res){

	var response = {};

	if( !req.user ){
		res.status(401).json({ 
			loggedIn: false,
			redirect: "/"
		});
		return;
	}


	res.status(200).json({ 
		loggedIn: true, 
		error: false, 
		user: req.user,  
		redirect: "/orders"
	});

});

module.exports = app;