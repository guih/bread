var fs 			= require('fs');
var db		 	= require('./models/db');
var _id			= null;
var _ 			= require('lodash');
var bread		= require('./models/bread');
var util 		= require('util');
var utils 		= require('./lib/util');
var async 		= require('async');
var email   	= require("emailjs/email");
var CronJob 	= require('cron').CronJob;
var mongoose	= require('mongoose');
var staticVars 	= require('./lib/static');
var breadlist 	= mongoose.model('BreadList');


exports.init = function(cb){
	var _that = this;

	async.series({
	    notice: function(fn){
			var notice = new CronJob(staticVars.timeToSendRule, function(){
				console.log('Init...')

				breadlist.create({  
					title: +new Date(), 
					time: new Date(),
					current: [],
					completed: false
				}, function(error, bread){
					if( error ){
						console.log(error);
					}
					id = bread._id;
					_that.write(id, function(stdout){});
				});

				_that.enqueue( staticVars.users, 'notice', function(){
					fn();
				});
			}, null,  null, staticVars.TIMEZONE);

			notice.start();
	    },
	},
	function(error, results) {
		if( error ){
			console.log(error);
		}
		var mail = new CronJob(staticVars.timeToEndRule, function(){
			console.log('Send final email...')
			_that.sendFinalEmail(function(){
				breadlist.findById(id, function (err, bread) {
					if( err ){
						console.log(err);
					}
					breadlist.find({_id: id}, function(err, Breads) {
						var Bread = Breads[0];
						Bread.completed = true;
						Bread.save(function(err) {
							if (err) throw err;

							console.log(Bread);
						});
					});

				});
			});
		}, null,  null, staticVars.TIMEZONE);
		mail.start();
		console.log('Emails sent!');

	});

}
exports.enqueue = function(list, type, fn){

	var i = 0;
	var _that = this;
	async.whilst(
	    function () { return i <= list.length; },
	    function (fn) {
	        i++;
	        if( list[i] ){
	        	setTimeout(function () {
	        		if( list[i].email == "guilherme.rodrigues@apiki.com" )
	        			console.log('Sending email to %s...', list[i].email)
				    
				    _that.sendEmail( list[i], type, function(end){
	        			// console.log(' ✔ Sent email to %s', list[i].email)
				    	
				            fn(null, i);
				        
			    	} ) 
		    	}, 100);		       	
	        }else{
				fn(null, i);
	        }
	    },
	    function (err, result) {
	    	fn(result);
	    }
	);	
}
exports.send = function(to, msg, fn){

	var target = util.format( "%s <%s>", to.name, to.email );

	console.log('Sending email to %s...', to.email);
	var server  = email.server.connect({
		user		: 	staticVars.apiKey, 
		password	: 	staticVars.apiSecret, 
		host		:   staticVars.apiEndpoint, 
		ssl			:   true
	}).send({
		subject		: "Vai querer pão hoje?",
		from 		: "Apiki Bot <bot@apiki.com>", 
		to 			: target,
		attachment	: [
			{
				data: msg, 
				alternative: true, 
				type: "text/html"
			},
		]
	}, function(err, message) { 
		fn(err || message); 
	});	
}

exports.readFileAsync = function(fileName, fn){
	fs.readFile(fileName, function (err, data) {
		if (err) throw err;
		fn(data);
	});
}

exports.sendEmail = function(to, type, fn){
	var tlp = "";
	var _that = this;

	this.readFileAsync( './templates/'+type+".html", function(res){

		var text = util.format( res.toString(),
			utils.btoa( to.email +';'+ id +';'+ 'sweet'  ), 
			utils.btoa( to.email +';'+ id +';'+ 'salt'  ),
			new Date()
		);
		// console.log(to.email, 'http://localhost:1337/confirm/'+utils.btoa( to.email +';'+ id +';'+ 'sweet'  ))
		// fn();

		_that.send(to, text, function(stdout){
			fn( stdout );
		});
	} );
}
exports.write = function(text, fn){
	var fs = require('fs');
	fs.appendFile("./quee.txt", text+"\n", function(err) {
	    if(err) {
	        return console.log(err);
	    }

	    fn("The file was saved!");
	}); 
}
exports.sendFinalEmail = function(fn){
	var _that = this;
	var flavors = {
		salt: 0,
		sweet: 0
	};
	var to = {email: "jane.berbert@apiki.com", name: "Jane"};

	require('fs').readFile('./quee.txt', function (err, data) {
		if (err) throw err;

		var lines = data.toString().split("\n");
		var last = lines[lines.length-2];
		console.log(last)
		breadlist.findById(last, function(err, breadList){
			var source = breadList.current;
			var peopleCount = source.length;

			for(var i = 0; i< peopleCount; i++){
				if( source[i].type == 'salt' ){
					flavors.salt++;
					continue;
				} 
				flavors.sweet++;
				if( i+1 == peopleCount ){
					console.log('Sohuld send email...')
					_that.readFileAsync( "./templates/email.html", function(res){

						var text = util.format( res.toString(),
							salt,
							sweet
						);

						_that.send(to, text, function(stdout){
							fn( stdout );
							console.log('email sent!.')

						});
					} );
				}
			}
		});

	});

};